---
layout: post
title:  "Drawing boxes with UTF-8"
date:   2024-03-15 22:32
categories: utf-8 box drawing blocks ascii-art
tags: ascii-art unicode-art tables box-drawing utf8 ascii
---

`tl;dr`




See the Wikipedia page for [Box-drawing characters](https://en.wikipedia.org/wiki/Box-drawing_characters).

---

┌─┬─┐  
│ │ │  
├─┼─┤  
│ │ │  
└─┴─┘  

━┃┄┆┅┇┈┊┉┋╌╍╎╏╴╵╶╷╸╹╺╻╼╽╾╿

┍┑┎┒┏┓  
┕┙┖┚┗┛  

┝┞┟┠┡┢┣┥┦┧┨┩┪┫  
┭┮┯┰┱┲┳┵┶┷┸┹┺┻  
┽┾┿╀╁╂╃╄╅╆╇╈╉╊╋  

═║  
╒╓╔╕╖╗  
╘╙╚╛╜╝  
╞╟╠╡╢╣╤╥╦╧╨╩╪╫╬  

╭╮  
╰╯  
╱ ╲ ╳  