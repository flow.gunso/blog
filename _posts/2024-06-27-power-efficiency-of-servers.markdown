---
layout: post
title:  "Power efficiency of servers"
date:   2024-06-27 16:29
categories: 
tags: 
---

`tl;dr`

# Resources

- [7 watts idle on Intel 12th/13th gen: the foundation for building a low power server/NAS](https://mattgadient.com/7-watts-idle-on-intel-12th-13th-gen-the-foundation-for-building-a-low-power-server-nas/)
- [The Curse of ASPM](https://z8.re/blog/aspm)
